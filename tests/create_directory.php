<?php
    require_once __DIR__ . '/../vendor/autoload.php';

    use fluid\fileManager\Directory;

    $dir = new Directory("testDir");
    $dir->create(true);
