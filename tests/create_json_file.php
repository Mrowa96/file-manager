<?php
    require_once __DIR__ . '/../vendor/autoload.php';

    use fluid\fileManager\File;

    $file = new File("testFile.json");
    $file->create(true)->write(json_encode(['key' => 'value']));