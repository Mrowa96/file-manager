<?php
    namespace fluid\fileManager;

    class File extends Base
    {
        public $extension = null;
        public $category = null;
        public $thumbnail = null;

        public function create($override = false)
        {
            $this->checkExistence();
            $this->handleExistence($override);

            if(fopen($this->path, 'w')){
                $this->checkVisibility();
                $this->exists = true;
            }

            return $this;
        }
        public function remove()
        {
            $this->onCall();

            if(unlink($this->path)){
                $this->exists = false;
            }

            return $this;
        }
        public function write($data, $override = false){
            $this->onCall();
            $existingData = $this->read();

            try{
                if($data && is_string($data)){
                    if($existingData !== $data || $existingData === null){
                        if($override === true || $existingData === null){
                            $file = fopen($this->path, 'w');
                            fwrite($file, $data);
                        }
                        else{
                            throw new \Exception("File cannot be overridden.");
                        }
                    }
                }

                return $this;
            }
            catch(\Exception $ex){
                echo $ex;
            }
        }
        public function read(){
            $this->onCall();

            try{
                $file = fopen($this->path, 'r');
                $fileSize = filesize($this->path);

                if($fileSize > 0){
                    return fread($file, filesize($this->path));
                }
                else{
                    return null;
                }
            }
            catch(\Exception $ex){
                echo $ex;
            }
        }
        public function copy($newPath, $override = false){
            $this->copyOrMove($newPath, "copy", $override);

            return $this;
        }
        public function move($newPath, $override = false){
            $this->copyOrMove($newPath, "rename", $override);

            return $this;
        }
        public function saveUploadedFile($fileData, $override = false){
            if($this->exists && $override === false){
                throw new \Exception("File with given name already exists.");
            }
            else if(empty($fileData)){
                throw new \Exception("Cannot localize uploaded file.");
            }
            else if($fileData['error'] !== 0){
                switch($fileData['error']){
                    case 1:
                        $message = "The uploaded file exceeds the upload_max_filesize directive.";
                        break;
                    case 2:
                        $message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.";
                        break;
                    case 3:
                        $message = "The uploaded file was only partially uploaded.";
                        break;
                    case 4:
                        $message = "No file was uploaded.";
                        break;
                    case 6:
                        $message = "Missing a temporary folder.";
                        break;
                    case 7:
                        $message = "Failed to write file to disk.";
                        break;
                    case 8:
                        $message = "Unknown error.";
                        break;
                    default:
                        $message = "Unknown error.";
                        break;
                }

                throw new \Exception($message);
            }
            else{
                if(isset($fileData["tmp_name"])){
                    $tempName = $fileData["tmp_name"];
                }
                else if(isset($fileData["tempName"])){
                    $tempName = $fileData["tempName"];
                }
                else{
                    throw new \Exception("Temp name is missing.");
                }

                if(!move_uploaded_file($tempName, $this->path)){
                    throw new \Exception("Server failed with moving file from temporary location.");
                }
                else{
                    return new File($this->path);
                }
            }
        }

        public static function is($path, $newInstance = false){
            parent::handleDirectorySeparator();
            $path = parent::handlePath($path);

            $result = (file_exists($path) && is_file($path));

            if($newInstance === true){
                if($result === true){
                    return new Directory($path);
                }
                else{
                    return $result;
                }
            }
            else{
                return $result;
            }
        }

        private function copyOrMove($newPath, $operation, $override = false){
            $file = new File($newPath);

            if($file->exists === true){
                if($override){
                    $operation($this->path, $file->path);
                }
                else{
                    throw new \Exception("Object with this name already exists.");
                }
            }
            else{
                $operation($this->path, $file->path);
            }
        }
    }
