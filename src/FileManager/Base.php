<?php
    namespace fluid\fileManager;

    abstract class Base
    {
        public $name;
        public $basename;
        public $path;
        public $parentPath;
        public $permissions;
        public $writable;
        public $hidden;
        public $type; // 0 - dir, 1 - file
        public $size;
        public $exists;
        protected $system; // 0 - Linux, 1 - Windows
        protected $categories = [
            'image' => ['png', 'jpg', 'bmp', 'gif', 'tiff'],
            'music' => ['mp3', "wma", "aac", "flac"],
            'video' => ['avi', "mp4", "mkv"],
            'document' => ['docx', 'pdf', 'txt'],
            "other" => ['*']
        ];

        public function __construct($value)
        {
            $this->checkRequirements();
            $this->detectSystem();
            $this->handleDirectorySeparator();
            $this->handleObject($value);
            $this->checkVisibility();
            $this->checkExistence();
            $this->handleSize();
            $this->handlePermissions();
            $this->handleCategory();
        }

        abstract public function create();
        abstract public function remove();
        abstract public function copy($newPath);
        abstract public function move($newPath);

        public function permissions($value)
        {
            $this->onCall();

            chmod($this->path, $value);

            return $this;
        }
        public function hide()
        {
            $this->onCall();

            if($this->hidden === false) {
                switch ($this->system) {
                    case 0:
                        $this->rename("." . $this->name);
                        break;
                    case 1:

                        break;
                }

                $this->hidden = true;
            }

            return $this;
        }
        public function show()
        {
            $this->onCall();

            if($this->hidden === true){
                switch($this->system){
                    case 0:
                        $this->rename(mb_substr($this->name, 1));
                        break;
                    case 1:

                        break;
                }

                $this->hidden = false;
            }

            return $this;
        }
        public function rename($newName)
        {
            $this->onCall();

            rename($this->path, $this->parentPath . DS . $newName);
            $this->path = $this->parentPath . DS . $newName;
            $this->name = basename($this->path);
            $this->parentPath = dirname($this->path);
            $this->checkVisibility();

            return $this;
        }

        protected static function handlePath($value){
            while(mb_strripos($value, "/") === (mb_strlen($value) - 1)){
                $value = mb_substr($value, 0, mb_strlen($value) - 1);
            }

            if(mb_strpos($value, "/") === 0){
                while(mb_strpos($value, "/") === 0){
                    $value = mb_substr($value, 1);
                }

                $value = "/" . $value;
            }
            else{
                $value = getcwd() . DS . $value;
            }

            return $value;
        }

        protected function onCall()
        {
            if($this->exists === false){
                throw new \Exception("Object does not exist.");
            }
            else{
                $this->checkVisibility();
                $this->checkExistence();
                $this->handleSize();
                $this->handlePermissions();
                $this->handleCategory();
            }
        }
        protected function checkRequirements()
        {
            if (!extension_loaded('imagick')){
                throw new \Exception("Imagick extension must be installed.");
            }
        }
        protected function handleObject($value)
        {
            $this->path = self::handlePath($value);
            $this->parentPath = dirname($this->path);
        }
        protected function detectSystem()
        {
            $system = mb_substr(mb_strtolower(PHP_OS), 0, 3);

            if($system === "win"){
                $this->system = 1;
            }
            else{
                $this->system = 0;
            }
        }
        protected function handleDirectorySeparator()
        {
            if(defined("DS") === false){
                define("DS", DIRECTORY_SEPARATOR);
            }
        }
        protected function checkVisibility()
        {
            switch($this->system){
                case 0:
                    if(mb_substr($this->name, 0, 1) === "."){
                        $this->hidden = true;
                    }
                    else{
                        $this->hidden = false;
                    }
                    break;
                case 1:

                    break;
            }
        }
        protected function checkExistence()
        {
            $this->exists = file_exists($this->path);

            if($this->exists === true && !$this->type){
                $this->type = (is_dir($this->path)) ? 0 : 1;

                if($this->type === 1){
                    $data = pathinfo($this->path);
                    $fileInfo = pathinfo($this->path);

                    $this->basename = $data['basename'];
                    $this->name = $data['filename'];

                    if(property_exists($this, "extension")){
                        $this->extension = (isset($fileInfo['extension'])) ? $fileInfo['extension'] : null;
                    }
                }
                else{
                    $this->name = basename($this->path);
                    $this->basename = $this->name;

                    if(property_exists($this, "objects")){
                        $this->objects = array_values(array_diff(scandir($this->path), ['.', '..']));
                    }
                }
            }
        }
        protected function handleSize($path = null)
        {
            if($this->exists === true){
                switch($this->type){
                    case 0:
                        $totalSize = 0;
                        $path = ($path === null) ? $this->path : $path;
                        $files = array_values(array_diff(scandir($path), ['.', '..']));

                        foreach($files as $object) {
                            $currentFile = $path . DS . $object;

                            if (is_dir($currentFile)) {
                                $size = $this->handleSize($currentFile);
                                $totalSize += $size;
                            }
                            else {
                                $size = filesize($currentFile);
                                $totalSize += $size;
                            }
                        }

                        if($path === $this->path){
                            $this->size = [
                                'byte' => $totalSize,
                                'human' => $this->bytesToStandard($totalSize)
                            ];

                            break;
                        }
                        else{
                            return $totalSize;
                        }

                    case 1:
                        $totalSize = filesize($this->path);
                        $this->size = [
                            'byte' => $totalSize,
                            'human' => $this->bytesToStandard($totalSize)
                        ];

                        break;
                }
            }
        }
        protected function handleExistence($override = false)
        {
            if($this->exists === true){
                if($override === true){
                    if(method_exists($this, "remove")){
                        $this->remove();
                    }
                }
                else{
                    throw new \Exception("Object with this name already exists.");
                }
            }
        }
        protected function handlePermissions()
        {
            if($this->exists === true){
                $this->permissions = substr(sprintf('%o', fileperms($this->path)), -4);
                $this->writable = is_writable($this->path);
            }
        }
        protected function bytesToStandard($bytes)
        {
            $bytes = floatval($bytes);
            $result = 0;

            $arBytes = array(
                0 => array(
                    "unit" => "TB",
                    "value" => pow(1024, 4)
                ),
                1 => array(
                    "unit" => "GB",
                    "value" => pow(1024, 3)
                ),
                2 => array(
                    "unit" => "MB",
                    "value" => pow(1024, 2)
                ),
                3 => array(
                    "unit" => "KB",
                    "value" => 1024
                ),
                4 => array(
                    "unit" => "B",
                    "value" => 1
                ),
            );

            foreach($arBytes as $arItem) {
                if($bytes >= $arItem["value"]) {
                    $result = $bytes / $arItem["value"];
                    $result = str_replace(".", "," , strval(round($result, 2))) . " " . $arItem["unit"];

                    break;
                }
            }
            return $result;
        }
        protected function handleCategory()
        {
            if($this->exists === true && $this->type === 1){
                if(property_exists($this, "category") && property_exists($this, "extension")){
                    foreach($this->categories AS $category => $type){
                        if(in_array($this->extension, $type)){
                            $this->category = $category;
                        }
                    }

                    if(!$this->category){
                        $this->category = "other";
                    }

                    if($this->category === "image" && mb_strpos($this->name, ".thumb") === false){
                        $thumbPath = $this->parentPath . DS . $this->name . '.thumb.' . $this->extension;

                        if(!File::is($thumbPath)){
                            $image = new \Imagick($this->path);
                            $image->thumbnailImage(100, 100, true, true);
                            $image->writeImage($thumbPath);
                        }

                        $this->thumbnail = new File($thumbPath);
                    }
                }
            }
        }
    }
